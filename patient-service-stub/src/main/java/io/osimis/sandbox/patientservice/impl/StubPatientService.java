package io.osimis.sandbox.patientservice.impl;

import io.osimis.sandbox.patientservice.api.PatientService;

public class StubPatientService implements PatientService {
    @Override
    public String findPatient(String search) {
        return "78256";
    }
}
