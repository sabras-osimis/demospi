package io.osimis.sandbox.demospi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    @Autowired
    PatientServiceProvider provider;

    @GetMapping("/hello")
    public String hello() {
        final var patientService = provider.get();

        return patientService.findPatient("anything");
    }
}
