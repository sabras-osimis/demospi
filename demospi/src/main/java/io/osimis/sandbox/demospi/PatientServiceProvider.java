package io.osimis.sandbox.demospi;

import io.osimis.sandbox.patientservice.api.PatientService;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.ServiceLoader;
import java.util.function.Supplier;

@Component
public class PatientServiceProvider implements Supplier<PatientService> {
    private ServiceLoader<PatientService> loader;

    public PatientServiceProvider() {
        this.loader = ServiceLoader.load(PatientService.class);
    }

    @Override
    public PatientService get() {
        return loader
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No patient service implementation found"));
    }
}
