package io.osimis.sandbox.demospi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemospiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemospiApplication.class, args);
	}

}
