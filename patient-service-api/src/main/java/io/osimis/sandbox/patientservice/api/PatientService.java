package io.osimis.sandbox.patientservice.api;

public interface PatientService {
    String findPatient(String search);
}
